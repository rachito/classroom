from rest_framework import routers
from . import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'user-answer', views.UserAnswerViewSet)
router.register(r'user-lesson', views.UserLessonViewSet)
router.register(r'approved-users', views.ApprovedCourseViewSet)

urlpatterns = []
urlpatterns += router.urls
