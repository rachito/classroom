# Generated by Django 2.0.7 on 2018-07-29 18:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('results', '0003_auto_20180729_1316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useranswer',
            name='snap_score',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10),
        ),
    ]
