from django.contrib.auth.models import User, Group
from rest_framework import viewsets

from .models import UserAnswer, UserLesson, ApprovedCourse
from .serializers import UserAnswerSerializer, UserSerializer, GroupSerializer, UserLessonSerializer, \
    ApprovedCourseSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Students to be viewed or edited.
    """
    # a = User.objects.get(pk=1).userlesson_set
    queryset = User.objects.filter(groups__name__in=['students']).order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class UserAnswerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = UserAnswer.objects.all()
    serializer_class = UserAnswerSerializer


class UserLessonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows userLesson to be viewed or edited.
    """
    queryset = UserLesson.objects.all()
    serializer_class = UserLessonSerializer


class ApprovedCourseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows ApprovedCourse to be viewed or edited.
    """
    queryset = ApprovedCourse.objects.all()
    serializer_class = ApprovedCourseSerializer
