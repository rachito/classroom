from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import UserAnswer, UserLesson, ApprovedCourse


class UserSerializer(serializers.HyperlinkedModelSerializer):
    approvedcourse_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='approvedcourse-detail'
    )
    userlesson_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='userlesson-detail'
    )

    class Meta:
        model = User
        fields = ('url', 'first_name', 'last_name', 'username', 'email', 'approvedcourse_set', 'userlesson_set')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class UserAnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserAnswer
        fields = ('url', 'user', 'answer', 'created_at')


class UserLessonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserLesson
        fields = ('url', 'user', 'lesson', 'is_approved')


class ApprovedCourseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ApprovedCourse
        fields = ('url', 'user', 'course')
