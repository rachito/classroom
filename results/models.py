from django.contrib.auth.models import User
from django.db import models

from courses.models import Answer, Lesson, Course


class UserAnswer(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    answer = models.ForeignKey(Answer, on_delete=models.DO_NOTHING)
    snap_score = models.DecimalField(max_digits=10, decimal_places=2, blank=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.get_full_name()} - Q: {self.answer.question.text}: R={self.answer.text}"

    def save(self, *args, **kwargs):
        lesson = self.answer.question.lesson
        question_score = self.answer.question.score
        self.snap_score = question_score
        user_lesson = UserLesson.objects.filter(user=self.user, lesson=lesson)
        if user_lesson.exists():
            ul = user_lesson[0]
            if self.answer.is_correct:
                ul.snap_score += question_score
            else:
                ul = user_lesson[0]
        else:
            ul = UserLesson(user=self.user, lesson=lesson)
            if self.answer.is_correct:
                ul.snap_score = question_score
        super(UserAnswer, self).save(*args, **kwargs)
        # Call de save after save UserAnswer
        ul.save()


class UserLesson(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    lesson = models.ForeignKey(Lesson, on_delete=models.DO_NOTHING)
    snap_score = models.DecimalField(max_digits=10, decimal_places=2, blank=True, default=0)
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user.get_full_name()} - {self.lesson.name}"

    def save(self, *args, **kwargs):
        # Get the total questions of the lesson
        lesson_questions = self.lesson.questions.count()
        # Get the total questions anwered by the user
        user_anwered_questions = UserAnswer.objects.filter(user=self.user).values('answer__question').distinct().count()
        if lesson_questions == user_anwered_questions:
            if self.snap_score >= self.lesson.approval_score:
                self.is_approved = True
        super(UserLesson, self).save(*args, **kwargs)
        ApprovedCourse.set_approved_course(self.lesson.course, user=self.user)

    class Meta:
        unique_together = (('user', 'lesson'),)


class ApprovedCourse(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING)

    def __str__(self):
        return f"{self.user.get_full_name()} - {self.course.name}"

    @staticmethod
    def set_approved_course(course, user):
        course_lessons = course.lessons.count()
        approved_lessons = UserLesson.objects.filter(user=user, is_approved=True).count()

        if course_lessons == approved_lessons:
            ac = ApprovedCourse(user=user, course=course)
            ac.save()

    class Meta:
        unique_together = (('user', 'course'),)
