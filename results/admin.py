from django.contrib import admin
from .models import UserAnswer, UserLesson, ApprovedCourse

admin.site.register(UserAnswer)
admin.site.register(UserLesson)
admin.site.register(ApprovedCourse)
