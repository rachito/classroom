from rest_framework import serializers
from .models import Course, Lesson, Question, Answer


class CourseSerializer(serializers.ModelSerializer):
    lessons = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='lesson-detail'
    )

    class Meta:
        model = Course
        fields = ('url', 'name', 'description', 'professor', 'students', 'sort', 'previous_course', 'lessons')


class LessonSerializer(serializers.HyperlinkedModelSerializer):
    questions = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='question-detail'
    )

    class Meta:
        model = Lesson
        fields = ('url', 'course', 'name', 'sort', 'previous_lesson', 'total_score', 'approval_score', 'questions')


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    answers = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='answer-detail'
    )

    class Meta:
        model = Question
        fields = ('url', 'lesson', 'text', 'type', 'sort', 'score', 'answers')


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer
        fields = ('url', 'text', 'question', 'is_correct', 'sort')
