from django.contrib.auth.models import User
from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=140)
    description = models.TextField()
    professor = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="professor")
    students = models.ManyToManyField(User, blank=True, related_name="students")
    sort = models.PositiveSmallIntegerField()
    previous_course = models.ForeignKey('self', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f"{self.name} - {self.professor.get_full_name()}"

    class Meta:
        ordering = ["sort", "professor"]


class Lesson(models.Model):
    course = models.ForeignKey(Course, related_name="lessons", on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=140)
    sort = models.PositiveSmallIntegerField()
    previous_lesson = models.ForeignKey('self', on_delete=models.DO_NOTHING, blank=True, null=True)
    total_score = models.DecimalField(max_digits=10, decimal_places=2)
    approval_score = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.course.name} - {self.name}"

    class Meta:
        ordering = ["course", "sort"]


class Question(models.Model):
    TYPES = (
        ('TF', 'True or False'),
        ('MCH1A', 'Multichoice one answer'),
        ('MCH2ORM', 'Multichoice two or more answer'),
        ('MCH2ALLC', 'Multichioce all mut be correct'),
    )
    lesson = models.ForeignKey(Lesson, related_name="questions", on_delete=models.DO_NOTHING)
    text = models.TextField()
    type = models.CharField(max_length=10, choices=TYPES)
    sort = models.PositiveSmallIntegerField()
    score = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        truncate_len = 30
        truncate = self.text
        if len(truncate) > truncate_len:
            truncate = f"{self.text[:truncate_len]}..."
        return f"{self.lesson.name} - {truncate}"

    class Meta:
        ordering = ["sort"]


class Answer(models.Model):
    text = models.TextField()
    question = models.ForeignKey(Question, related_name="answers", on_delete=models.DO_NOTHING)
    is_correct = models.BooleanField(default=False)
    sort = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.question.text} - {self.text}"

    class Meta:
        ordering = ["question"]
