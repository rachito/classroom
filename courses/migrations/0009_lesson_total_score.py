# Generated by Django 2.0.7 on 2018-07-29 16:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0008_auto_20180727_1630'),
    ]

    operations = [
        migrations.AddField(
            model_name='lesson',
            name='total_score',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
    ]
