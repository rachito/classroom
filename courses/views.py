from rest_framework import viewsets
from .serializers import CourseSerializer, LessonSerializer, QuestionSerializer, \
    AnswerSerializer
from .models import Course, Lesson, Question, Answer


class CourseViewSet(viewsets.ModelViewSet):
    """ API endpoint that allows Courses to be viewed or edited"""
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class LessonViewSet(viewsets.ModelViewSet):
    """API endpoint that allows Lessons to be viewed or edited"""
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer


class QuestionViewSet(viewsets.ModelViewSet):
    """API endpoint that allows Questions to be viewed or edited"""
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class AnswerViewSet(viewsets.ModelViewSet):
    """API endpoint that allows Answers to be viewed or edited"""
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
