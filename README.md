## Instalación

1. Clona el repositorio: `git clone git@bitbucket.org:rachito/classroom.git`
2. La rama a usar es: *master*
3. Crea un entorno virtual con virtualenv y activalo con los comando correspondientes (Paso opcional)
4. Instala las dependencias del proyecto con  `pip install -r requirements.txt`
5. Una vez instaladas todas las dependencias ejecuta el comando `python manage.py runserver`
6. La aplicación quedará corriendo en: http://127.0.0.1:8000

**Nota**: Para fines prácticos se versionaron: el archivo settings.py y el archivo de base de datos de sqlite.

  

### Urls de proyecto
**API Docs:** http://127.0.0.1:8000/api/docs

**Admin:** http://127.0.0.1:8000/admin

### Usuarios
**administrador**: 
*usuario*: developers, *contraseña*: holamundo
**Profesor**
*usuario*: profesor, *contrseña*: holamundo
**Estudiante**
*usuario*: estudiante, *contraseña*: holamundo

---

  

## Manual de uso
Por default existe en el sistema un curso, que tiene una lección, la cual tiene 2 preguntas y cada pregunta tiene sus determinadas respuestas.

 - Para responder una lección se debe hacer un post al endpoint: **user-answer** con el usuario y respuesta (de una determinada pregunta de una determinada lección de un determinado curso.)
 - De forma automática se crea una **user-lesson** con su determinada puntuación de acuerdo a las respuestas dadas (correctas o incorrectas).
 - Tambien de forma automática y solo cuando un usuario completa todas sus lecciones y las aprueba (**todas**), se crea un **approved-course** al usuario correspondiente.
---

## To Do's
  La siguiente lista de tareas quedaron pendientes debido al tiempo que se tenía para realizar la prueba, pero cuentas con los requerimientos mínimos que hacen posible alcanzar el objetivo que requiere el cliente en este caso un sistema de cursos, que permite a un profesor publicar cursos y a un alumno responderlo y completar lecciones y cursos.

 - Validación al crear tipos de pregunta.
 - Validaciones para evitar responder varias veces una misma pregunta.
 - Endpoint que permite enviar en lote todas las respuestas de una determinada lección cursada por un alumno.
---

## Frameworks utilizados

**Django**
La razón principal de utilizar este framework es por la velocidad en la que te permite realizar aplicaciones, por su documentación de fácil consulta y también por todo el soporte que tiene por parte de la comunidad.

**Django Rest Framework**
Se utilizó por la integración que tiene con django y a parte de que sigue una misma linea en cuanto a la forma de hacer las cosas.